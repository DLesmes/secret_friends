# secret_friends

a [code](https://gitlab.com/DLesmes/secret_friends/-/blob/main/generator.py) to generate a random selection of the secret friends properly, this are the [requirements](https://gitlab.com/DLesmes/secret_friends/-/blob/main/requirements.txt) for the pip env

Here is a sample of the [Gsheet](https://docs.google.com/spreadsheets/d/1y3m99k7HP9t_xbNk93FweBFwNn8pmhC1QxRcgVIurrM/edit#gid=0) you can use for the game

It have this structure:

|nombre|correo|regalo|
|---|---|---|
|Nombre de la persona participante|Correo electronico de la persona participante|Opciones de regalo de su preferencia dentro del presupuesto mínimo establecido|

Remember to change the main mail and the link to the gsheet you use for the Game
